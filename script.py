from flask import Flask, render_template , request
import numpy as np
import pickle 
import joblib
import pandas as pd 
import os 

app = Flask('__name__')
final_model = joblib.load('RandomForestRegressor.pkl')
data_preparation = joblib.load('pipline.pkl')

@app.route('/')
def man():
    return render_template('index.html')

@app.route('/predict',methods=["POST"])

def predict():
    longitude = request.form.get('longitude')
    latitude = request.form.get('latitude')
    housing_median_age = request.form.get('housing_median_age')
    total_rooms = request.form.get('total_rooms')
    total_bedrooms = request.form.get('total_bedrooms')
    population = request.form.get('population')
    households = request.form.get('households')
    median_income = request.form.get('median_income')
    ocean_proximity = request.form.get('ocean_proximity')
    rooms_per_household = float(total_rooms)/ float(households)
    bedrooms_per_room = float(total_bedrooms)/ float(total_rooms)
    population_per_household = float(population)/ float(households)
    features = np.array([longitude, latitude, housing_median_age, total_rooms, population, households,median_income,ocean_proximity, rooms_per_household,bedrooms_per_room, population_per_household])
    df_feat=pd.DataFrame(data=[features], columns=['longitude', 'latitude', 'housing_median_age', 'total_rooms','population',
     'households', 'median_income','ocean_proximity', 'rooms_per_household', 'bedrooms_per_room', 'population_per_household'])
    clean_features= data_preparation.transform(df_feat)
    prediction = final_model.predict(clean_features)
    return render_template('index.html', prediction_text='the price of your house :{}'.format(prediction))

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=os.environ.get('PORT'))

